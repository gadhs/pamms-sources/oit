= 1905 Identification and Authentication Policy
:policy-number: 1905
:policy-title: Identification and Authentication Policy

include::partial$policy-header.adoc[]

== Policy

This policy establishes the Enterprise Identification and Authentication Policy, for managing risks from user access (organizational, non-organizational) and authentication into company information assets through the establishment of an effective identification and authentication program.
The identification and authentication program helps DHS implement security best practices with regards to identification and authentication into company information assets.

== Authority

. United States Department of Commerce National Institute for Standards and Technology (NIST)
. Georgia Technology Authority
. United States Internal Revenue Service
. United States Department of Health & Human Services
. Centers for Medicare & Medicaid Services

== References

* https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-53r4.pdf[United States Department of Commerce National Institute for Standards and Technology (NIST) Special Publication 800-53 Security and Privacy Controls for Federal Information Systems and Organizations, Revision 4, January 2015]
* https://doi.org/10.6028/NIST.SP.800-73-4[United States Department of Commerce National Institute for Standards and Technology (NIST) Special Publication 800-73 “Interfaces for Personal Identity Verification - PIV Card Application Namespace, Data Model and Representation” Revision 4 May 2015]
* https://doi.org/10.6028/NIST.SP.800-63-3[United States Department of Commerce National Institute for Standards and Technology (NIST) Special Publication 800-63 “Digital Identity Guidelines” December 2017]
* https://doi.org/10.6028/NIST.SP.800-76-2[United States Department of Commerce National Institute for Standards and Technology (NIST) Special Publication 800-76 “Biometric Specifications for Personal Identity Verification” Revision 2 July 2013]
* https://gta-psg.georgia.gov/article/information-technology-policies-and-standards[Georgia Technology Authority Enterprise Information Security Policy]
* https://www.cms.gov/CCIIO/Resources/Regulations-and-Guidance/Downloads/2-MARS-E-v2-0-Minimum-Acceptable-Risk-Standards-for-Exchanges-11102015.pdf[Centers for Medicare & Medicaid Services, Volume II: Minimum Acceptable Risk Standards for Exchanges]

== Applicability

The scope of this policy is applicable to all Information Technology (IT) resources owned or operated by DHS.
Any information, not specifically identified as the property of other parties, that is transmitted or stored on DHS IT resources (including e-mail, messages and files) is the property of DHS.
All users (DHS employees, contractors, vendors or others) of IT resources are responsible for adhering to this policy.

== Definitions

None

== Responsibilities

DHS shall adopt the Identification and Authentication principles established in NIST SP 800-53 “Identification and Authentication,” Control Family guidelines, as the official policy for this domain.
The following subsections outline the Identification and Authentication standards that constitute DHS policy.
Each DHS Business System is then bound to this policy, and shall develop or adhere to a program plan which demonstrates compliance with the policy related to the standards documented.

=== IA-1 Identification and Authentication Policy and Procedures

DHS shall:

. Senior management, management, and all organization entities are required to coordinate and implement necessary controls for providing identification and authentication controls to IT resources and information systems on the basis of business and security requirements.
. Periodic reviews of this policy shall be performed and documented at least within every *three years*, or when there is a *significant change*.
. Periodic review of identification and authentication procedures shall be performed at least *annually*.

=== IA-2 Identification and Authentication (Organizational User)

. DHS requires that all organizational users (or processes acting on behalf of users) are uniquely identified and authenticated prior to accessing agency information systems.
. Multi-factor authentication is required for:
.. All remote network access to privileged and non-privileged accounts for information systems that receive, process, store, or transmit FTI.
.. Remote access to privileged and non-privileged accounts such that one of the factors is provided by a device separate from the system gaining access.

=== IA-3 Device Identification and Authentication

DHS requires that all system devices uniquely identify and authenticate organizational users prior to the establishment of a connection.

=== IA-4 Identifier Management

DHS manages information system identifiers for users and devices by:

. Requiring and receiving authorization from designated organizational officials which authorize the assignment of a user, group, role, or device identifier.
. Selecting and assigning an identifier that uniquely identifies an individual, group, role, or device.
.. Assignment of individual, group, role, or device identifiers shall ensure that no two users or devices have the same identifier.
. Preventing the reuse of identifiers.
. Disabling and de-provisioning inactive user IDs after *120 days of inactivity*.
.. All mainframe system RACF IDs are revoked after *31 days of inactivity*.
.. Deleting all RACF User IDs that have been inactive, and or not used for more than *90 days* from the mainframe system

=== IA-5 Authenticator Management

. DHS manages information system authenticators by: Verifying the identity of the individual, group, role, or device receiving an information system authenticator as part of the initial authenticator distribution.
. Using unique initial authenticator content established for information system authenticators.
. Ensuring that authenticators have sufficient strength of mechanism for their intended use.
. Executing administrative procedures for initial authenticator distribution, for initial authenticator distribution, lost/compromised, or damaged authenticators, and for revoking authenticators.
.. If a user knows or suspects that their password has been compromised, they shall immediately:
... Notify their supervisor.
... Report a known or potential security breach to the GTA Helpdesk.
... Request the GTA Helpdesk reset or change their password, or if self-service password mechanisms are used, immediately change their own password.

. Prohibiting the use of automated tools for password generation.
. Ensuring that default content of authenticators (i.e., passwords provided for initial entry to a system) shall be changed before implementation of the information system or component (e.g., routers, switches, firewalls, printers, workstations, servers).
. Protecting authenticator content from unauthorized disclosure and modification.
. Requiring users to take, and having devices implement, specific measures to safeguard authenticators.
. Changing authenticators for group/role accounts when membership to those accounts changes.

. Enforcing minimum password complexity of:
.. A minimum of *8 characters*
.. At least *one* numeric and at least *one* special character
.. A mixture of at least *one* uppercase and at least *one* lowercase letter
.. Storing and transmitting only encrypted representation of passwords
.. A minimum lifetime restriction of *one day* is implemented.
Users are required to change privileged accounts within every *60 days*; non-privileged accounts within every *90 days*; all Mainframe account passwords are required to be changed within every *30 days*.

. The re-use of the *last 24 passwords* is prohibited
. Temporary passwords used for system logon are authorized, and are required to be changed immediately to a permanent password
. System initialization (boot) settings are password-protected

=== IA-6 Authenticator Feedback

The agency obscures feedback of authentication information during authentication processes to protect the information from possible exploitation/use by unauthorized individuals.

. Passwords shall be masked upon entry (e.g., displaying asterisks or dots when a user types in a password) and not displayed in clear text.
. Feedback from the information system does not provide information that would allow an unauthorized user to compromise the authentication mechanism.

=== IA-7 Cryptographic Module Authentication

. All DHS information systems implement approved mechanisms for authentication to a cryptographic module which meet applicable federal laws, Executive Orders, directives, policies, regulations, standards, and guidance for authentication.
. The encryption functions have been examined in detail and will operate as intended to protect sensitive data, such as, but not limited to, FTI and SSA data.
. All electronic transmissions of FTI is encrypted using FIPS 140-2 validated cryptographic modules.

=== IA-8 Identification and Authentication (Non-Organizational Users)

DHS requires that non-organizational users and processes acting on behalf of non-agency users uniquely identify and authenticate agency information systems.

== History

None

== Evaluation

include::partial$evaluation.adoc[]